var express = require('express');
var app = express();

var cors = require("cors")

app.use(cors())

// healthcheck
app.get('/', function (req, res) {
   console.log("Health check requested");
   res.send("OK");
})

app.get('/api/testcasegroups', (req, res) => {
	console.log("Test case groups requested");
    res.sendFile( __dirname + "/testcasegroups.json" );
});

app.get('/api/testcasegroup/:testcasegroupid', (req, res) => {
	// For the URI: /light/17/off
	console.log(`Test case group request ${req.params}`);
	res.sendFile( __dirname + `/testcasegroup-${req.params.testcasegroupid}.json` );
});

var server = app.listen(8081, function () {
   var port = server.address().port
   
   console.log("Test API server listening at http://localhost:%s", port)
})