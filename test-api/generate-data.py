from datetime import datetime
from datetime import timedelta
from random import choices

pop = [0, 1, 2]
wts = [.86, .07, .07]

cases = []
for case in range(0, 5):
   cases.append(choices(pop, wts, k=513))

start = '2022-01-01 00:00:00'
fmt = "%Y-%m-%d %H:%M:%S"

start = datetime.strptime(start, fmt)
now = start + timedelta(minutes=240)

results = [0] * 3

for i in range(0, 513):
   row = [f"\"{start + timedelta(minutes=(i*240))}\""]
   c = [0] * 3
   for case in cases:
      row.append(str(case[i]))
      c[case[i]] = c[case[i]] + 1

   results[0] = results[0] + c[0]/5
   results[1] = results[1] + c[1]/5
   results[2] = results[2] + c[2]/5
   out = ",".join(row)
   print(f"      [{out}],")

print([round(x, 0) for x in results])
