Lens Action Centre currently has a total of 21 different statuses, all of which are grouped into three categories.
 
The categories are what we’ve been using for the main categories for Pass/Warning/Failure in the reports.
 
Here is a list of the three different categories, and what statuses fit under each category.
 
**Pass**
+ Pass
+ No Reference
+ Reference
 
**Warning**
+ Warning
+ Latency
 
**Failure**
+ Fail
+ Stopped
+ Syntax Error
+ No Input
+ No Match
+ Timeout
+ No Answer
+ Busy
+ Telco Error

