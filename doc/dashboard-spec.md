# Dashboard Specs - preliminary

<img src="dashboard.png" >
<figcaption>Fig.1 - Main Dashboard</figcaption>


## Excerpt of sample data returned from API:
```json
{
    "code": "success",
    "data": [
        {
            "entityId": 16481,
            "entityName": "Manulife Group Benefit - 877-405-1992",
            "entityType": "case",
            "periodStart": "2022-01-01T00:00:00",
            "periodEnd": "2022-01-01T23:59:59.999999999",
            "successCount": 87,
            "warningCount": 9,
            "failureCount": 1,
            "hourlyStatus": [
                {
                    "hourly": "2022-01-01T01:00:00",
                    "successCount": 3,
                    "warningCount": 1,
                    "failureCount": 0
                },
                {
                    "hourly": "2022-01-01T02:00:00",
                    "successCount": 1,
                    "warningCount": 3,
                    "failureCount": 0
                },
                .
                .
                .
```


## Specifications

For an Entity ID:
Total test run = successCount + warningCount + failureCount
% of each status = statusCount / total test run * 100
>Hourly counts are breakdowns of the counts for each entity. 
>Sum of hourly counts = entity’s counts.

+ A: Sum of all entities’ success count / Sum of all entities’ total test run
+ B: Sum of all entities’ total test run
+ C: Sum of all entities’ success count, and basing (B) for the corresponding %
+ D: Sum of all entities’ warning count, and basing (B) for the corresponding %
+ E: Sum of all entities’ failure count, and basing (B) for the corresponding %
+ F: Filter for the first column in the table
+ G: Filter for the second column in the table
+ H: Count and % at entity level depending on the filter selection
+ I: Hourly graph per entity based on the filter selection
+ J: Same as H 