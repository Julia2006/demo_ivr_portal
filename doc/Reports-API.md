Develop API to support UI operations on reports.

- [x] List of reports
- [x] Get a particular report
- [x] Create a new report
- [x] Update an existing report
- [x] Delete an existing report
- [x] List of report types (for report config)
- [x] List of report time frames (for report config)

## API call
### List of reports
#### Endpoint 

> `GET /api/trs/v1/reports`

#### Input data
> `None`

#### Return body
> `Connex Standard where data = array of Report Structure`

### Get a particular report
#### Endpoint 

> `GET /api/trs/v1/reports/{reportId}`

#### Input data
> `reportId`: report id

#### Return body
> `Connex Standard where data = Report Structure`

### Create a new report
#### Endpoint 

> `POST /api/trs/v1/reports`

#### Input data
> `request body = Report Structure`

#### Return body
> `Connex Standard where data = Report Structure`


### Update an existing report
#### Endpoint 

> `PUT /api/trs/v1/reports/{reportId}`

#### Input data
> `reportId`: report id
> `request body = Report Structure`

#### Return body
> `Connex Standard where data = Report Structure`

### Delete an existing report
#### Endpoint 

> `DELETE /api/trs/v1/reports/{reportId}`

#### Input data
> `reportId`: report id

#### Return body
> `Connex Standard where data = NONE`

### List of report types (for report config)
#### Endpoint 

> `GET /api/trs/v1/reports/types`

#### Input data
> `NONE`

#### Return body
> `Connex Standard where data = array of Report Type Structure`

### List of report time frames (for report config)
#### Endpoint 

> `GET /api/trs/v1/reports/timeframes`

#### Input data
> `NONE`

#### Return body
> `Connex Standard where data = array of strings`

## API Data Model
### Connex Standard
>
>```json
> {
>    "code": <success,fail> 
>    "data": {<whatever is required>}
>    "message": <info, warning, error>,
>    "result": <true, false>
>}
>```

### Report Structure
>
>```json
> {
>    "id": <report id>
>    "name": <report name>
>    "description": <report description>
>    "reportTypeID": <report type id>
>    "testCaseGroupID": <test case group id>
>    "timeframeName": <Today, Yesterday, Custom>
>    "timeframeBegin": <start date in YYYY-MM-DD>
>    "timeframeEnd": <end date in YYYY-MM-DD>
>    "latencyWarn": <latency threshold for warning>
>    "latencyFail": <latency threshold for failure>
>}
>```

### Report Type Structure
>
>```json
> {
>    "id": <report type id>
>    "name": <report type name>
>    "description": <report type description>
>}
>```

