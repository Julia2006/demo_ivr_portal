Unified list of the different Adobe XD mock ups for all of the pages being created for the Lens Action Center

[Login Page](https://xd.adobe.com/view/6f31ffa5-22e2-4e83-a568-26b30366d70e-3a62/)

[Dashboard](https://xd.adobe.com/view/389b47c7-4629-4ba3-a1d0-ecb23aa4204d-6dc8/)

Report Management
+ [Manage Reports](https://xd.adobe.com/view/65892f11-e5dc-4217-a64b-6ccf57527cee-30da/)
+ [Create Report](https://xd.adobe.com/view/65892f11-e5dc-4217-a64b-6ccf57527cee-30da/screen/ca96f18a-2a79-4f24-b68f-963e39ae9f4c)
+ [Edit Report](https://xd.adobe.com/view/65892f11-e5dc-4217-a64b-6ccf57527cee-30da/screen/e310f576-fcd8-4a72-84d3-c87726c981a1)

Reports
+ [Test Segment Performance](https://xd.adobe.com/view/42c2e9cc-a6f8-4893-923c-413f8f192f8e-1791/)
+ [Test Case Performance](https://xd.adobe.com/view/b4709e64-a667-412f-9b2b-8930eeac23b4-e123/)
+ [Test Case Status](https://xd.adobe.com/view/4eccb503-9f0b-4015-8efd-9c2912406608-1b9f/)
+ [Test Sgement Latency by Hour](https://xd.adobe.com/view/47aba5cf-7358-48c4-a2df-30863ee163f6-12f8/)
+ [Drill Down](https://xd.adobe.com/view/f4409965-b07a-4a42-8eb4-1a485ed8325b-a113/)
+ [Reporting Prototype](https://xd.adobe.com/view/4316c712-1da2-4ec6-9315-1eb669c42c8f-1645/)

Alerts
+ [Alerts](https://xd.adobe.com/view/e144eabc-a95e-4086-801b-1431c3d0cf26-b417/)

Settings
+ [Alert Settings](https://xd.adobe.com/view/6319347c-29f1-442d-9503-119ab7224004-364a/)
+ [Test Case Groups](https://xd.adobe.com/view/c48e9597-af8a-4901-8a2c-bad07ff9ab86-91dd/)
+ [Distribution Lists](https://xd.adobe.com/view/c3452a0c-88f1-4842-ab21-f08642b41c8d-9616/)
+ [Admin Settings](https://xd.adobe.com/view/edd501b4-38c2-4ec1-a785-e3f9085f2843-b886/)
+ [User Profile](https://xd.adobe.com/view/7dd071b2-a8e6-4a6d-a857-ba9156afe1df-2883/ )

Mobile
+ [Dashboard](https://xd.adobe.com/view/89063743-43d0-4fcb-86ee-f9d56ca5caf9-dc55/)
+ [Alerts](https://xd.adobe.com/view/35b40ae0-1d49-41d4-a5e1-bb6d2139bbb3-60e7/)
+ [User Profile](https://xd.adobe.com/view/c769320e-5c05-4566-8316-817de2ea85e9-923a/)
+ [Alert Settings](https://xd.adobe.com/view/d6370fcd-bb31-4a43-b655-7c91bc602bab-8364/)
