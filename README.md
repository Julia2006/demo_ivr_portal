# Tekvision Lens - Action Centre

[Wireframes Index](doc/LensActionCenterWireframes.md)
[Dashboard Specifications](doc/dashboard-spec.md)

## Overview
TekVision's Lens Action Center is intuitively designed to be the visual hub for your CX Monitoring test results including user definable dashboards, detailed reports, plus audio captures of interactions between our CX Monitoring solution and your contact center systems.

## Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### Prerequisites
+ nodejs v16*
+ npm v8*
### Development

After cloning this repository, in the project directory, run:

##### `npm start`

which runs the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.  The page will reload if you make edits and lint errors in the console.

