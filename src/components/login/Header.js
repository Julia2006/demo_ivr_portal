import {
    Box,
    FormControl,
    MenuItem,
    Select,
    Toolbar,
    Typography,
} from "@mui/material";
import tekvision from "../../images/tekvision.png";
import React, {useState} from "react";
import {makeStyles} from "@mui/styles";

const useStyles = makeStyles((theme) => ({
    page: {
        opacity: 1,
    },
    logo: {
        display: "flex",
        justifyContent: "space-between",
    },
    logoLg: {
        display: "none",
        [theme.breakpoints.up("sm")]: {
            display: "block",
        },
    },
    login: {
        color: "#707070",
    },
}));

const Header = () => {
    const classes = useStyles();
    const [language, setLanguage] = useState("English");
    const handleChange = (event) => {
        setLanguage(event.target.value);
    };
    return (
        <Toolbar className={classes.logo}>
            <Typography variant="h6" className={classes.logoLg}>
                <img src={tekvision} alt="pic"/>
            </Typography>
            {/* <Box sx={{minWidth: 120}}>
                <FormControl fullWidth>
                    <Select
                        value={language}
                        onChange={handleChange}
                        displayEmpty
                        inputProps={{"aria-label": "Without label"}}
                    >
                        <MenuItem value={"English"}>English</MenuItem>
                        <MenuItem value={"French"}>French</MenuItem>
                    </Select>
                </FormControl>
            </Box> */}
        </Toolbar>
    );
};

export default Header;
