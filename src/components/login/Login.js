import React, { useState, useEffect } from "react";
import { isMobile } from 'react-device-detect';
import {
    Box,
    Checkbox,
    Container,
    Divider,
    FormControlLabel,
    FormHelperText,
    Grid,
    InputAdornment,
    TextField,
    Typography,
} from "@mui/material";
import { makeStyles } from "@mui/styles";
import EmailIcon from "@mui/icons-material/Email";
import LockOpenIcon from "@mui/icons-material/LockOpen";
import LoadingButton from "@mui/lab/LoadingButton";
import lensLogo from "../../images/Lens Logo Teal E.svg"
import Header from "./Header";
import { useDispatch, useSelector } from "react-redux";
import { login, ShowGlobalErrorAlert } from "../../actions";
import { useHistory } from "react-router-dom";
import Popup from "../../helper/Popup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import SuccessAlert from "../mui/SuccessAlert";

const useStyles = makeStyles((theme) => ({
    page: {
        opacity: 1,
    },
    textAlign: {
        position: "relative",
        top: "10px",
        color: "#1f9997",
    },
    logofl: {
        color: "#636364",
        fontSize: "36px",
        opacity: 1,
        fontFamily: "Montserrat",
    },
    logo: {
        display: "flex",
        justifyContent: "space-between",
    },
    logoLg: {
        display: "none",
        [theme.breakpoints.up("sm")]: {
            display: "block",
        },
    },
    formBox: {
        minWidth: "300px",
    },
    login: {
        color: "#707070",
        paddingTop: '1.5rem',
    },
}));

const schema = yup.object().shape({
    email: yup.string().required("Required").trim(),
    password: yup.string().required("Required").trim(),
});

const Login = () => {
    const classes = useStyles();
    const history = useHistory();
    const dispatch = useDispatch();
    const [checked, setChecked] = useState(false);
    const [open, setOpen] = useState(false);
    const [loading, setLoading] = useState(false);
    const [showErrorAlert, setShowErrorAlert] = useState(false);
    const loginResult = useSelector((state) => state.loginReducer.loginResult);
    const globalErrorAlert = useSelector(state => state.reportReducer.globalErrorAlert);

    const handleSaveSubmit = (data) => {
        setLoading(true);
        login({
            username: data.email,
            password: data.password,
            remember: checked,
        })(dispatch);
    };


    const handleChange = (event) => {
        setChecked(event.target.checked);
    };

    useEffect(() => {
        setLoading(false);
        if (loginResult && loginResult?.data && loginResult.data.username) {
            document.location.href = ("/dashboard");
        }
        if (loginResult && loginResult.message) {
            setOpen(true);
            setTimeout(function () {
                setOpen(false);
            }, 2500);
        }
    }, [loginResult]); // eslint-disable-line react-hooks/exhaustive-deps


    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
    } = useForm({
        resolver: yupResolver(schema),
    });

    useEffect(() => {
        if (globalErrorAlert) {
            setLoading(false);
            setShowErrorAlert(true);

            setTimeout(() => {
                ShowGlobalErrorAlert(null)(dispatch);
            }, 6000);
        } else {
            setShowErrorAlert(false);
        }

    }, [globalErrorAlert]);

    useEffect(() => {
        const loggedInUser = localStorage.getItem("user");

        if (loggedInUser) {
            const userData = JSON.parse(loggedInUser);
            if (userData) {
                setChecked(userData.remember);

                if (userData.remember) {
                    reset({
                        email: userData?.data?.username,
                        password: '',
                    })
                }
            }
        }
    }, []);

    useEffect(() => {
        document.body.style.backgroundColor = "#EFEFEF";
    }, []);



    return (
        <div className={classes.page}>
            <Header/>
            <br/>
            <br/>
            <Container component="main" maxWidth="md">
                <Box
                    sx={{
                        minWidth: isMobile ? "0" : "600px",
                        padding: "50px",
                        display: "flex",
                        flexDirection: "column",
                        alignItems: "center",
                        borderWidth: "3px",
                        borderStyle: "solid",
                        borderColor: "lightgrey",
                        borderRadius: "20px",
                        backgroundColor: "white",
                    }}
                >
                    <Grid item>
                        <img src={lensLogo} alt="Action Center" width="100px"/> 
                    </Grid>
                    <Grid item >
                        <div className={classes.logofl}>Action Center</div>
                    </Grid>
                    <Grid item xs>
                        <Typography component="h4" variant="h4" className={classes.login}>
                            Login
                        </Typography>
                        <br />

                        <Divider
                            sx={{
                                "&.MuiDivider-root": {
                                    borderTop: `thick solid #1f9997`,
                                },
                            }}
                        ></Divider>
                    </Grid>

                    <Box
                        className={classes.formBox}
                        component="form"
                        onSubmit={handleSubmit(handleSaveSubmit)}
                        noValidate
                        sx={{ mt: 1 }}
                    >
                        <Grid item className={classes.textAlign}>
                            <span>Username</span>
                        </Grid>
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            name="email"
                            autoComplete="email"
                            placeholder="john@example.com"
                            autoFocus
                            InputLabelProps={{
                                style: { fontSize: 15, fontWeight: "bold", color: "grey" },
                            }}
                            {...register("email")}
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <EmailIcon sx={{ color: "grey", fontSize: 20 }} />
                                    </InputAdornment>
                                ),
                            }}
                        />
                        <FormHelperText
                            error={Boolean(errors.email?.message)}
                            style={{ fontSize: 12 }}
                        >
                            {errors.email?.message}
                        </FormHelperText>
                        <Grid item className={classes.textAlign}>
                            <span>Password</span>
                        </Grid>
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                            placeholder="*********"
                            InputLabelProps={{
                                style: { fontSize: 15, fontWeight: "bold", color: "grey" },
                            }}
                            {...register("password")}
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <LockOpenIcon sx={{ color: "grey", fontSize: 22 }} />
                                    </InputAdornment>
                                ),
                            }}
                        />
                        <FormHelperText
                            error={Boolean(errors.password?.message)}
                            style={{ fontSize: 12 }}
                        >
                            {errors.password?.message}
                        </FormHelperText>

                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={checked}
                                    onChange={handleChange}
                                    inputProps={{ "aria-label": "controlled" }}
                                    sx={{
                                        color: "#1f9997",
                                        "&.Mui-checked": {
                                            color: "#1f9997",
                                        },
                                    }}
                                />
                            }
                            label="Remember me"
                        />
                        <Box textAlign="center">
                            <LoadingButton
                                loading={loading}
                                type="submit"
                                variant="contained"
                                sx={{ mt: 3, mb: 2 }}
                                style={{
                                    borderRadius: 5,
                                    backgroundColor: "#1f9997",
                                    fontSize: "18px",
                                    width: "200px",
                                    textTransform: "none",
                                }}
                            >
                                Login
                            </LoadingButton>
                        </Box>
                    </Box>
                </Box>
            </Container>
            {loginResult && loginResult.message && (
                <Popup message={loginResult.message} open={open} />
            )}

            {showErrorAlert && (
                <SuccessAlert width="80%" setOpenAlert={setShowErrorAlert} top="10px" openAlert={showErrorAlert} statement={globalErrorAlert || ''} fail="error" />
            )}
        </div>
    );
};

export default Login;
