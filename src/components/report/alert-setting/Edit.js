import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { useParams } from "react-router-dom";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { Controller, useForm } from "react-hook-form";
import ContentHeader from "../../mui/ContentHeader";
import { Box, MenuItem, TextField, Typography, Button, Checkbox, FormControlLabel, Grid } from "@mui/material";
import { getAlertSetting, getAlertStatuses, getSettingsReports, upsertAlertSettings } from "../../../actions";
import { ReportTypes } from "../../../helper/report-helper";
import Modal from "../../mui/Modal";
import Schedule from "./Schedule";
import { isMobile } from 'react-device-detect';

const schema = yup.object().shape({
    label: yup.string().required("Required").trim(),
    testCaseGroupId: yup.string().required("Required").trim(),
    contactGroupId: yup.string().required("Required").trim(),
});


const EditContent = () => {
    const dispatch = useDispatch();
    let history = useHistory()
    const { id } = useParams();
    const [hash, setHash] = useState("init");
    const [editOpen, setEditOpen] = useState(false);
    const testCaseGroup = useSelector(state => state.reportReducer.testCaseGroup);
    const distributions = useSelector(state => state.reportReducer.distributions);
    const alertSetting = useSelector(state => state.reportReducer.alertSetting);
    const alertStatuses = useSelector(state => state.reportReducer.alertStatuses);

    const {
        register,
        handleSubmit,
        setError,
        formState: { errors },
        reset,
        control,
        setValue,
        getValues,
    } = useForm({
        resolver: yupResolver(schema),
        reValidateMode: 'onChange',
        defaultValues: {
            label: '', testCaseGroupId: 0, contactGroupId: 0, notiActive: false,
            email: '', voice: '', sms: '', attachRecording: false, noFailures: 0,
            alertRecovery: false, warnings: false, alertStatus: [], schedule: []
        }
    });

    const scheduled = (data) => {
        setValue('schedule', data);
        setEditOpen(false);
    }

    const handleSaveSubmit = (data) => {

        if (!data.label || !data.testCaseGroupId || !data.contactGroupId
            || !data.email || !data.voice || !data.sms || !data.alertStatus) {
            return;
        }

        let model = {
            name: data.label,
            active: data.notiActive,
            testCaseGroupId: data.testCaseGroupId,
            distributionId: data.contactGroupId,
            emailSubject: data.email,
            attachRecording: data.attachRecording,
            voiceMessage: data.voice,
            smsPrefix: data.sms,
            failureToAlert: data.noFailures,
            notifyOnRecovery: data.alertRecovery,
            alertStatus: data.alertStatus,
            schedule: data.schedule,
        };

        if (id > 0) {
            model = {
                ...model,
                id,
            }
        }

        upsertAlertSettings(model, +id)(dispatch);
        history.push('/settings/alert-settings/1');

    }

    const updateStatus = (e) => {
        const values = getValues('alertStatus')

        if (e.target.checked) {
            setValue('alertStatus', (values || []).concat(e.target.value))
        } else {
            setValue('alertStatus', values.filter(v => v !== e.target.value))
        }

    }
    useEffect(() => {
        if (alertSetting) {
            reset({
                label: alertSetting.name,
                testCaseGroupId: alertSetting.testCaseGroupId,
                contactGroupId: alertSetting.distributionId,
                notiActive: alertSetting.active,
                noFailures: +alertSetting.failureToAlert,
                email: alertSetting.emailSubject,
                alertRecovery: alertSetting.notifyOnRecovery,
                attachRecording: alertSetting.attachRecording,
                voice: alertSetting.voiceMessage,
                sms: alertSetting.smsPrefix,
                alertStatus: alertSetting.alertStatus,
                schedule: alertSetting.schedule,
            });
        }
    }, [alertSetting])

    useEffect(() => {
        getSettingsReports(ReportTypes.TEST_CASE_GROUP)(dispatch)
        getSettingsReports(ReportTypes.DISTRIBUTION)(dispatch)
        getAlertStatuses()(dispatch);

        if (id > 0) {
            getAlertSetting(id)(dispatch);
        } else {
            reset({
                label: '',
                testCaseGroupId: 0,
                contactGroupId: 0,
                notiActive: false,
                noFailures: 0,
                email: '',
                alertRecovery: false,
                attachRecording: false,
                voice: '',
                sms: '',
                alertStatus: '',
                schedule: [],
            });
        }
    }, [id])


    return (
        <div style={isMobile ? { paddingBottom: '65px', paddingLeft: '24px', paddingRight: '24px', paddingTop: '0' } : { position: "absolute", top: "0px", left: "130px", width: `calc(100% - 200px)` }}>
            <ContentHeader title={`SETTINGS / ALERT SETTINGS / ${id > 0 ? 'EDIT' : 'CREATE'} ALERT SETTING`} />

            <Box textAlign='left'
                sx={{
                    minWidth: isMobile ? "100%" : "500px",
                    borderColor: 'lightgrey',
                    borderRadius: '5px',
                    borderWidth: '1px',
                    borderStyle: 'solid',
                    backgroundColor: '#1F9997',
                    marginBottom: '5px',
                    verticalAlign: 'middle'
                }}
            >
                <Typography type="body2" className="edit-title">
                    {id > 0 ? 'Edit' : 'Create'} Alert Setting
                </Typography>

            </Box>

            <form onSubmit={handleSubmit(handleSaveSubmit)} style={isMobile ? { width: 'calc(100vw - 50px)' } : {}}>
                <Box sx={{ mt: 4, mb: 5 }}>
                    <Controller
                        render={({ field }) => (
                            <TextField size="small" label="Status Alert Label*"
                                fullWidth={isMobile}
                                {...field} value={field.value}

                            />
                        )}
                        control={control}
                        name="label"
                    />
                </Box>
                <Box sx={{ mt: 3, mb: 3 }}>
                    <Grid container>
                        <Grid item xs={12}>
                            <Typography variant="h6">
                                Details
                            </Typography>
                        </Grid>
                        <Grid item xs={isMobile ? 12 : 4} sx={{ mt: 3 }}>
                            <Controller
                                render={({ field }) => (
                                    <TextField select size="small" label="Test Case Group*" className="min-w-340"
                                        fullWidth={isMobile}
                                        {...field} value={field.value}>
                                        <MenuItem value={0}>Select</MenuItem>
                                        {testCaseGroup?.map(tcg => (
                                            <MenuItem key={tcg.id} value={tcg.id}>{tcg.name}</MenuItem>
                                        ))}
                                    </TextField>
                                )}
                                control={control}
                                name="testCaseGroupId"
                            />
                        </Grid>
                        <Grid item xs={isMobile ? 12 : 6} sx={{ mt: 3 }}>
                            <Controller
                                render={({ field }) => (

                                    <TextField select size="small" label="Contact Group*" className="min-w-340"
                                        {...field} value={field.value}
                                    >
                                        <MenuItem value={0}>Select</MenuItem>
                                        {distributions?.map(tcg => (
                                            <MenuItem key={tcg.id} value={tcg.id}>{tcg.name}</MenuItem>
                                        ))}
                                    </TextField>
                                )}
                                control={control}
                                name="contactGroupId"
                            />
                        </Grid>
                    </Grid>
                </Box>
                <Typography variant="h6">
                    Notification Options
                </Typography>

                <Grid container sx={{ mb: 4 }}>
                    <Grid item xs={isMobile ? 12 : 3}>
                        <Box sx={{ mt: isMobile ? 2 : 0 }}>
                            <Typography variant="body2" className="config">
                                Configuration
                            </Typography>
                            <Controller
                                render={({ field }) => (
                                    <FormControlLabel
                                        className="checkbox-label" control={<Checkbox checked={field.value}  {...field} />} label="Active" />
                                )}
                                control={control}
                                name="notiActive"
                            />
                            <Button sx={{ ml: 2 }} variant="contained" color="primary" className="round-button" onClick={() => setEditOpen(true)}>Schedule</Button>
                        </Box>
                    </Grid>
                    {isMobile ? (
                        <>
                            <Grid item xs={12}>
                                <TextField InputProps={{
                                    readOnly: true,
                                }} size="small" label="Type*" defaultValue={"Email"} className="mt-30" fullWidth />
                            </Grid>
                            <Grid item xs={12}>
                                <Controller
                                    render={({ field }) => (
                                        <TextField size="small" label="Subject" fullWidth
                                            {...field} value={field.value} className="mt-30"

                                        />
                                    )}
                                    control={control}
                                    name="email"
                                />
                            </Grid>

                            <Grid item xs={12}>
                                <TextField InputProps={{
                                    readOnly: true,
                                }} size="small" label="Type*" defaultValue={"Voice"} className="mt-30" fullWidth />
                            </Grid>
                            <Grid item xs={12}>
                                <Controller
                                    render={({ field }) => (
                                        <TextField size="small" label="Message" fullWidth
                                            {...field} value={field.value} className="mt-30"

                                        />
                                    )}
                                    control={control}
                                    name="voice"
                                />
                            </Grid>

                            <Grid item xs={12}>
                                <TextField InputProps={{
                                    readOnly: true,
                                }} size="small" label="Type*" defaultValue={"SMS"} className="mt-30" fullWidth />
                            </Grid>
                            <Grid item xs={12}>
                                <Controller
                                    render={({ field }) => (
                                        <TextField size="small" label="Message" fullWidth
                                            {...field} value={field.value} className="mt-30"

                                        />
                                    )}
                                    control={control}
                                    name="sms"
                                />
                            </Grid>
                        </>
                    ) : (
                        <>
                            <Grid item xs={3}>
                                <Box>
                                    <TextField InputProps={{
                                        readOnly: true,
                                    }} size="small" label="Type*" defaultValue={"Email"} className="mt-30" />
                                    <TextField InputProps={{
                                        readOnly: true,
                                    }} size="small" label="Type*" defaultValue={"Voice"} className="mt-30" />
                                    <TextField InputProps={{
                                        readOnly: true,
                                    }} size="small" label="Type*" defaultValue={"SMS"} className="mt-30" />
                                </Box>
                            </Grid>
                            <Grid item xs={3}>
                                <Box>
                                    <Controller
                                        render={({ field }) => (
                                            <TextField size="small" label="Subject"
                                                {...field} value={field.value} className="mt-30"

                                            />
                                        )}
                                        control={control}
                                        name="email"
                                    />
                                    <Controller
                                        render={({ field }) => (
                                            <TextField size="small" label="Message"
                                                {...field} value={field.value} className="mt-30"

                                            />
                                        )}
                                        control={control}
                                        name="voice"
                                    />
                                    <Controller
                                        render={({ field }) => (
                                            <TextField size="small" label="Message"
                                                {...field} value={field.value} className="mt-30"

                                            />
                                        )}
                                        control={control}
                                        name="sms"
                                    />
                                </Box>
                            </Grid>
                        </>
                    )}

                    <Grid item xs={isMobile ? 12 : 3}>
                        <Box sx={{ mt: 3 }} >
                            <Controller
                                render={({ field }) => (
                                    <FormControlLabel className="checkbox-label" control={<Checkbox  {...field} checked={field.value} />} label="Attach call recording" />
                                )}
                                control={control}
                                name="attachRecording"
                            />

                        </Box>
                    </Grid>
                </Grid>

                <Typography variant="h6">
                    Alert Conditions
                </Typography>

                <Grid container sx={{ mt: 2 }}>
                    <Grid item xs={isMobile ? 12 : 3}>
                        <Controller
                            render={({ field }) => (
                                <TextField sx={{ mt: 2 }} select label="Number of Failures *"
                                    className="min-w-240" size="small"
                                    {...field} value={field.value}>
                                    <MenuItem value={0}>Select</MenuItem>
                                    <MenuItem value={1}>1</MenuItem>
                                    <MenuItem value={2}>2</MenuItem>
                                    <MenuItem value={3}>3</MenuItem>
                                    <MenuItem value={5}>5</MenuItem>
                                    <MenuItem value={10}>10</MenuItem>
                                </TextField>
                            )}
                            control={control}
                            name="noFailures"
                        />

                    </Grid>
                    <Grid item xs={isMobile ? 12 : 3}>
                        <Box sx={{ mt: isMobile ? 3 : 0 }}>
                            <Typography variant="body2" className="config">
                                Alert Once and on Recovery
                            </Typography>
                            <Controller
                                render={({ field }) => (
                                    <FormControlLabel
                                        className="checkbox-label" control={<Checkbox checked={field.value}  {...field} />} label="Enabled" />
                                )}
                                control={control}
                                name="alertRecovery"
                            />
                        </Box>
                    </Grid>
                    <Grid item xs={isMobile ? 6 : 3}>
                        <Box sx={{ mt: isMobile ? 3 : 0 }}>
                            <Typography variant="body2" className="config">
                                Failures *
                            </Typography>
                            <Controller
                                render={({ field }) =>
                                (
                                    <>
                                        {alertStatuses?.filter(c => c.statusGroup === 'TRS_FAIL').map(status => (
                                            <FormControlLabel
                                                className="checkbox-label db"
                                                control={<Checkbox value={status.code}
                                                    onChange={updateStatus}
                                                    checked={field.value.indexOf(status.code) !== -1} />}
                                                label={status.text} />
                                        ))}
                                    </>
                                )
                                }
                                control={control}
                                name="alertStatus"
                            />

                        </Box>
                    </Grid>
                    <Grid item xs={isMobile ? 6 : 3}>
                        <Box sx={{ mt: isMobile ? 3 : 0 }}>
                            <Typography variant="body2" className="config">
                                Warnings
                            </Typography>
                            <Controller
                                render={({ field }) => (
                                    <>
                                        {alertStatuses?.filter(c => c.statusGroup === 'TRS_WARN').map(status => (
                                            <FormControlLabel
                                                className="checkbox-label db"
                                                control={<Checkbox value={status.code}
                                                    onChange={updateStatus}
                                                    checked={field.value.indexOf(status.code) !== -1} />}
                                                label={status.text} />
                                        ))}
                                    </>
                                )}
                                control={control}
                                name="alertStatus"
                            />
                        </Box>
                    </Grid>
                </Grid>
                <Box textAlign='left'
                    sx={{
                        minWidth: isMobile ? "100%" : "500px",
                        borderRadius: '5px',
                        borderWidth: '0',
                        borderStyle: 'solid',
                        backgroundColor: '#E8F3F4',
                        mt: isMobile ? 4 : 6,
                        mb: isMobile ? 2 : 0,
                    }}
                >
                    <Button
                        type="submit"
                        variant="contained"
                        sx={{ mt: 1, mb: 1, ml: 2 }}
                        style={{
                            borderRadius: 7,
                            backgroundColor: "#1f9997",
                            fontSize: "14px",
                            width: "190px",
                            height: 40
                        }}
                    >
                        <Typography style={{ fontFamily: 'Poppins', fontSize: 14, textTransform: 'none', color: "white" }}>Save</Typography>
                    </Button>

                    <Button

                    >
                        <Typography style={{ fontFamily: 'Poppins', fontSize: 14, fontWeight: 600, textTransform: 'none', color: "#BDBDBD" }}
                            onClick={() => history.push('/settings/alert-settings/1')} >Cancel</Typography>
                    </Button>
                </Box>
                <Modal
                    isOpen={editOpen}
                    openHash={hash}
                    overflow={true}
                    body={<Schedule data={getValues('schedule') || []} done={scheduled} cancel={() => setEditOpen(false)} />}
                />
            </form>
        </div>
    )
}

export default EditContent;