import React from "react";
import ContentDetail from "./reportList/ContentDetail";
import ContentHeader from "../mui/ContentHeader";

const ReportList = () => {
    const reports = "REPORTS"
  
    return (
        <div style={{position: "absolute", top: "0px", left: "130px", width: `calc(100% - 200px)`}}>
            <ContentHeader title={reports}/>
            <ContentDetail/>
        </div>
    );
};

export default ReportList;