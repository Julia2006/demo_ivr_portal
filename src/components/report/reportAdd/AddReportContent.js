import React, { forwardRef, useEffect, useState } from "react";
import {
    Box,
    Button, FormHelperText, InputLabel,
    MenuItem, Select, TextField, Typography
} from "@mui/material";
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { DatePicker as DatePickers, LocalizationProvider } from '@mui/x-date-pickers';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { makeStyles } from "@mui/styles";
import { Controller, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useHistory } from "react-router-dom";
import { addDays } from "date-fns";
import moment from "moment";
import Divider from "@mui/material/Divider";
import timeLogo from "../../../images/Group 196.svg";
import 'react-date-range/dist/styles.css'; // main css file
import 'react-date-range/dist/theme/default.css';
import ContentHeader from "../../mui/ContentHeader";
import { useDispatch, useSelector } from "react-redux";
import { createReport, editReport, getLatency, getReportTypes, groupsList, LensActions, toggleGlobalAlert } from "../../../actions";
import { addDay, currentMonth, currentMonths, currentWeek, currentWeeks, lastMonth, lastMonths, lastWeek, lastWeeks, lastYear, lastYears } from "../../../helper/date";

const useStyles = makeStyles((theme) => ({
    title: {
        color: "white",
        height: "40px",
        verticalAlign: "center",
        padding: "10px"
    },

    input: {
        marginRight: "20px",
        width: "250px",
        height: "40px",
        borderRadius: "5px",
        fontSize: "14px",
        border: "1px solid lightgrey",
        paddingLeft: '16px',
        "&:focus": {
            outline: "none !important",
            border: "2px solid #3f50b5",
        },
    }
}));

const schema = yup.object().shape({
    name: yup.string().required("Required").trim(),
});


const AddReportContent = ({ editMode = false }) => {
    const addReport = `REPROT / ${editMode ? 'Edit' : 'CREATE'} REPORT`
    const Custom = "Custom"
    const Latency = "Test Script Latency by Hour"
    const classes = useStyles();
    const dispatch = useDispatch()
    const history = useHistory()
    const [warning, setWarning] = useState('');
    const [failure, setFailure] = useState('')
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(addDays(new Date(), 6));
    const [form, setForm] = useState({
        timeFrame: 'Custom',
        type: '',
        group: '',
    });

    const onChange = (dates) => {
        if (!dates.length) {
            setStartDate(dates);
            setEndDate(dates);
            return;
        }
        const [start, end] = dates;
        setStartDate(start);
        setEndDate(end);
    };
    const reportTypes = useSelector(state => state.reportReducer.getReportTypes)
    const testGroups = useSelector(state => state.loginReducer.groupsList)
    const latency = useSelector(state => state.reportReducer.latency)
    const timeFrames = ["Custom", "Today", "Yesterday", "Current Week", "Last Week", "Current Month", "Last Month", "Last Year"];
    const getOneReport = useSelector(state => state.reportReducer.getOneReport);

    useEffect(() => {
        getReportTypes()(dispatch)
        groupsList()(dispatch)
        getLatency()(dispatch)
    }, []) // eslint-disable-line react-hooks/exhaustive-deps




    const {
        register,
        handleSubmit,
        setError,
        formState: { errors },
        control,
        reset
    } = useForm({
        resolver: yupResolver(schema),
        reValidateMode: 'onChange',
    });

    useEffect(() => {
        if (editMode && getOneReport && getOneReport?.data) {
            reset({
                name: getOneReport.data.name,
            })

            setForm({
                name: getOneReport.data.name,
                type: getOneReport.data.reportTypeID,
                timeFrame: getOneReport.data.timeframeName,
                group: getOneReport.data.testCaseGroupID,
            })
            const start = getOneReport.data.timeframeBegin.split('-');
            const end = getOneReport.data.timeframeEnd.split('-');
            setStartDate(new Date(start[0], (+start[1]) - 1, start[2], 0, 0, 0, 0))
            setEndDate(new Date(end[0], (+end[1]) - 1, end[2], 0, 0, 0, 0))
            setWarning(getOneReport.data.latencyWarn)
            setFailure(getOneReport.data.latencyFail)
        } else {
            setForm({
                name: '',
                type: '',
                timeFrame: 'Custom',
                group: '',
            })
        }
    }, [getOneReport, editMode])

    const handleChangeWarning = (event) => {
        setWarning(event.target.value)
    }
    const handleChangeFailure = (event) => {
        setFailure(event.target.value)
    }

    const handleSaveSubmit = ({ name }) => {

        if (!form?.type) {
            return setError('type', { type: 'manual', message: 'Please select type' })
        }
        if (!form?.group) {
            return setError('group', { type: 'manual', message: 'Please select group' })
        }

        if (form?.type && form?.type.name === Latency && !warning) {
            return setError('warning', { type: 'manual', message: 'Please select threshold warning' })
        }
        if (form?.type && form?.type.name === Latency && !failure) {
            return setError('failure', { type: 'manual', message: 'Please select threshold failure' })
        }

        let reportData = {}
        reportData.name = name
        reportData.description = name
        reportData.reportTypeName = form?.type
        reportData.reportTypeID = form?.type
        reportData.testCaseGroupName = form?.group
        reportData.testCaseGroupID = form?.group
        reportData.timeframeName = form?.timeFrame;

        if (form?.timeFrame === "Custom") {
            reportData.timeframeBegin = new moment(startDate).format('YYYY-MM-DD');
            reportData.timeframeEnd = endDate ? new moment(endDate).format('YYYY-MM-DD') : new moment(startDate).format('YYYY-MM-DD');
        } else {
            reportData.timeframeBegin = getStartEndTime()[0];
            reportData.timeframeEnd = getStartEndTime()[1];
        }


        reportData.latencyWarn = +form?.type === 6 ? warning : 0
        reportData.latencyFail = +form?.type === 6 ? failure : 0

        if (editMode) {
            editReport(reportData, getOneReport.data.id)(dispatch)
        } else {
            createReport(reportData)(dispatch)
        }
        history.push('/reports')
    };

    const cancel = () => {
        history.push(`/reports`)
        dispatch({ type: LensActions.CreateReport, payload: null })
        dispatch({ type: LensActions.EditReport, payload: null })
    }

    const handleFormChange = (e) => {
        setForm({
            ...form,
            [e.target.name]: e.target.value,
        })
    }
    const getStartEndTime = () => {
        switch (form?.timeFrame) {
            case "Today": return [new moment().format('YYYY-MM-DD'), new moment().format('YYYY-MM-DD')];
            case "Yesterday": return [new moment(addDay(new Date(), -1)).format('YYYY-MM-DD'), new moment(addDay(new Date(), -1)).format('YYYY-MM-DD')];
            case "Current Week": return currentWeeks(new Date());
            case "Last Week": return lastWeeks(new Date());
            case "Current Month": return currentMonths(new Date());
            case "Last Month": return lastMonths(new Date());
            case "Last Year": return lastYears(new Date());
        }
    }

    const rendertimeFrame = () => {
        switch (form?.timeFrame) {
            case "Custom": return `${new moment(startDate).format('MMM DD, YYYY')} - ${endDate ? new moment(endDate).format('MMM DD, YYYY') : ""}`;
            case "Today": return new moment().format('MMM DD, YYYY');
            case "Yesterday": return new moment(addDay(new Date(), -1)).format('MMM DD, YYYY');
            case "Current Week": return currentWeek(new Date());
            case "Last Week": return lastWeek(new Date());
            case "Current Month": return currentMonth(new Date());
            case "Last Month": return lastMonth(new Date());
            case "Last Year": return lastYear(new Date());
        }
    }

    const ExampleCustomInput = forwardRef(({ value, onClick }, ref) => (

        <Box sx={{
            display: 'flex',
            flexDirection: "column",
            alignItems: "flex-start",
            justifyContent: "flex-start",
            width: "270px"
        }}>

            <fieldset
                style={{
                    display: "flex",
                    alignItems: "center",
                    width: "250px",
                    height: "40px",
                    border: "1px solid lightgrey",
                    borderRadius: "5px",
                    marginTop: '-10px',
                    padding: '13px'
                }}>
                <legend style={{ fontSize: '14px', fontFamily: 'Montserrat', fontWeight: '400', color: 'rgba(0, 0, 0, 0.6)' }}>Select TimeFrame*</legend>

                <Typography
                    style={{ fontSize: 14, marginRight: "10px", color: "#636364", marginTop: "-5px", width: "200px" }}
                >
                    &nbsp;&nbsp;{rendertimeFrame()}
                </Typography>

                {form?.timeFrame === 'Custom' && (
                    <>
                        <Divider sx={{ height: 40, mr: 0.5, marginTop: '-10px' }} orientation="vertical" />
                        <img src={timeLogo} alt="Select Day" onClick={onClick} ref={ref}
                            style={{
                                backgroundColor: "#1F9997",
                                border: "3px solid #1F9997",
                                borderRadius: "5px",
                                width: "25px",
                                height: "25px",
                                marginTop: '-10px',
                                cursor: form?.timeFrame === 'Custom' ? 'pointer' : ''
                            }}
                        />
                    </>
                )}
            </fieldset>
        </Box>
    ));

    return (
        <div style={{ position: "absolute", top: "0px", left: "130px", width: `calc(100% - 200px)` }}>
            <ContentHeader title={addReport} />
            <Box textAlign='left'
                sx={{
                    minWidth: "500px",
                    borderColor: 'lightgrey',
                    borderRadius: '5px',
                    borderWidth: '1px',
                    borderStyle: 'solid',
                    backgroundColor: '#1F9997',
                    marginBottom: '20px'
                }}
            >
                <Typography type="body2" className={classes.title}>
                    {editMode ? 'Edit' : 'Create'} Report
                </Typography>

            </Box>

            {editMode && (
                <Typography variant="subtitle1" style={{
                    fontSize: '18px', fontWeight: 600, fontFamily: 'Montserrat'
                }}>
                    Report ID : {getOneReport?.data?.id}
                </Typography>
            )}

            <Box component="form"
                onSubmit={handleSubmit(handleSaveSubmit)}
            >
                <Box style={{ display: "flex", flexDirection: "row" }} sx={{ mt: 5 }}>

                    <div style={{ width: "400px", marginRight: 16 }}>
                        <Controller
                            render={({ field }) => (
                                <TextField size="small"
                                    label="Report Label*"
                                    style={{ minWidth: '300px' }}
                                    {...field} value={field.value}

                                />
                            )}
                            control={control}
                            name={`name`}
                        />

                        <FormHelperText
                            style={{ color: "red" }}>{Boolean(errors.name?.message) ? errors.name?.message : null}</FormHelperText>
                    </div>

                    <div style={{ width: "270px" }}>
                        <TextField
                            value={form?.type}
                            select
                            label="Report Type*"
                            onChange={handleFormChange}
                            name="type"
                            style={{ marginRight: "20px", width: "250px" }}
                            size="small"
                        >
                            {reportTypes?.data?.map((option, index) => (
                                <MenuItem key={index} value={option.id}>
                                    {option.name}
                                </MenuItem>
                            ))}
                        </TextField>

                        <FormHelperText
                            style={{ color: "red" }}>{Boolean(errors.type?.message) ? errors.type?.message : null}</FormHelperText>
                    </div>

                    <div style={{ width: "270px" }}>

                        <TextField
                            value={form?.group}
                            select
                            label="Test Case Group*"
                            onChange={handleFormChange}
                            name="group"
                            style={{ marginRight: "20px", width: "250px" }}
                            size="small"
                        >
                            {testGroups?.data?.map((option, index) => (
                                <MenuItem key={index} value={option.id}>
                                    {option.name}
                                </MenuItem>
                            ))}
                        </TextField>

                        <FormHelperText
                            style={{ color: "red" }}>{Boolean(errors.group?.message) ? errors.group?.message : null}</FormHelperText>
                    </div>
                    <div style={{ width: "270px" }}>

                        <TextField
                            value={form?.timeFrame}
                            select
                            label="Select Timeframe*"
                            onChange={handleFormChange}
                            style={{ marginRight: "20px", width: "250px" }}
                            name="timeFrame"
                            size="small"
                        >
                            {timeFrames.map((option, index) => (
                                <MenuItem key={index} value={option}>
                                    {option}
                                </MenuItem>
                            ))}
                        </TextField>

                        <FormHelperText
                            style={{ color: "red" }}>{Boolean(errors.group?.message) ? errors.group?.message : null}</FormHelperText>
                    </div>

                    {form?.timeFrame === 'Custom' ? (
                        <DatePicker
                            selected={startDate}
                            onChange={onChange}
                            customInput={<ExampleCustomInput />}
                            startDate={startDate}
                            endDate={endDate}
                            selectsRange
                        />
                    ) : (
                        <ExampleCustomInput />
                    )}
                </Box>

                <br /><br />

                <Box style={{ display: form?.type && form?.type === 6 ? "flex" : "none", flexDirection: "row" }}>
                    <div style={{ width: "350px" }}>
                        <TextField
                            select
                            label="Latency Threshold Warnings*"
                            value={warning}
                            onChange={handleChangeWarning}
                            style={{ marginRight: "20px", width: "300px" }}
                            size="small"
                        >
                            {(latency || []).map((option, index) => (
                                <MenuItem key={index} value={option.id}>
                                    {option.text}
                                </MenuItem>
                            ))}
                        </TextField>

                        <FormHelperText
                            style={{ color: "red" }}>{Boolean(errors.warning?.message) ? errors.warning?.message : null}</FormHelperText>
                    </div>

                    <div style={{ width: "350px" }}>

                        <TextField
                            value={failure}
                            select
                            label="Latency Threshold Failures*"
                            onChange={handleChangeFailure}
                            style={{ marginRight: "20px", width: "300px" }}
                            size="small"
                        >
                            {(latency || []).map((option, index) => (
                                <MenuItem key={index} value={option.id}>
                                    {option.text}
                                </MenuItem>
                            ))}
                        </TextField>

                        <FormHelperText
                            style={{ color: "red" }}>{Boolean(errors.failure?.message) ? errors.failure?.message : null}</FormHelperText>
                    </div>
                </Box>
                <br />
                <Box textAlign='left'
                    sx={{
                        minWidth: "500px",
                        backgroundColor: '#E8F3F4',
                        marginTop: form?.type && form?.type === Latency ? '100px' : '165px',
                        height: 72,
                        display: 'flex',
                        alignItems: 'center',
                    }}
                >
                    <Button
                        type="submit"
                        sx={{ mt: 1, mb: 1, ml: 5 }}
                        style={{
                            borderRadius: 5,
                            background: '#1F9997 0% 0% no-repeat padding-box',
                            height: 40,
                            width: "200px",
                        }}
                    >
                        <Typography style={{ textTransform: 'none', fontSize: "15px", fontFamily: 'Poppins', fontWeight: 600, color: "white" }}>Save</Typography>
                    </Button>

                    <Button
                        sx={{ ml: 2 }}
                        onClick={cancel}
                    >
                        <Typography style={{ textTransform: 'none', fontSize: '15px', fontFamily: 'Poppins', fontWeight: 600, color: "#BDBDBD" }}>Cancel</Typography>
                    </Button>
                </Box>
            </Box>
        </div>

    )
}

export default AddReportContent