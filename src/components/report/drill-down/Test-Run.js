import { Box, Typography, Grid, TableHead, Table, TableRow, TableCell, TableBody } from "@mui/material";
import { useEffect } from "react";
import { useParams } from "react-router-dom";
import LeftBar from "../../dashboard/LeftBar";
import Navbar from "../../dashboard/Navbar";
import ContentHeader from "../../mui/ContentHeader";
import { useDispatch, useSelector } from "react-redux";
import { downloadFile, getTestRunDetails } from "../../../actions";
import { formatDate, formatDateTime } from "../../../helper/date";
import downloadIcon from "../../../images/Download Icon.svg";
import { isMobile } from 'react-device-detect';

const TestRunResult = () => {
    const defaultPage = [0, 1, 0, 0];
    const dispatch = useDispatch();
    const { id, latency } = useParams();
    const testRunDetails = useSelector(state => state.reportReducer.testRunDetails);
    const statuses = useSelector(state => state.reportReducer.alertStatuses);

    const formatOutput = (run, columns) => {
        let output = columns;
        Object.keys(run).forEach(key => {
            if (columns.indexOf(key) !== -1) {
                output = `${output.replace(key, run[key])} `;
            }
        });

        return output;
    }

    useEffect(() => {
        if (id) {
            getTestRunDetails(+id)(dispatch);
        }
    }, [id]);

    return (
        <div>
            <Navbar />
            <Grid container spacing={3} style={isMobile ? {padding:'16px'}:{}}>
                {!isMobile && (
                    <Grid item>
                        <LeftBar page={defaultPage} />
                    </Grid>
                )}
                <Grid item xs={12}>
                    <div style={isMobile ? { } : { position: "absolute", top: "0px", left: "130px", width: `calc(100% - 200px)` }}>
                        <ContentHeader title={`TEST RUN DETAILS`} />

                        <Box textAlign='left'
                            sx={{
                                minWidth: isMobile ? "100%" : "500px",
                                borderColor: 'lightgrey',
                                borderRadius: '5px',
                                borderWidth: '1px',
                                borderStyle: 'solid',
                                backgroundColor: '#1F9997',
                                marginBottom: '5px',
                                verticalAlign: 'middle'
                            }}
                        >
                            <Typography type="body2" className="edit-title">
                                Test Run Details
                            </Typography>

                        </Box>

                        <Box sx={{ mt: 5, ml: 3 }}>
                            <Grid container className={isMobile ? "mobile-alert-settings" : ''}>
                                <Grid xs={isMobile ? 12 : (latency ? 4 : 6)}>
                                    <Typography variant="h7">{latency ? 'Test Segment ID' : 'Test Case ID'}:</Typography>
                                    <Typography variant="h8">#{testRunDetails?.testCaseId}</Typography>
                                </Grid>
                                <Grid xs={isMobile ? 12 : (latency ? 4 : 6)}>
                                    <Typography variant="h7">{latency ? 'Test Segment Name' : 'Test Case Name'}:</Typography>
                                    <Typography variant="h8">{testRunDetails?.testCaseName}</Typography>
                                </Grid>
                                {latency && (
                                    <Grid xs={latency ? 4 : 6} sx={{ textAlign: 'right' }}>
                                        <Typography variant="h7">Test Run Status:</Typography>
                                        <Typography variant="h8">{statuses?.find(s => s.id === testRunDetails?.runStatusId)?.text}</Typography>
                                    </Grid>
                                )}
                                <Grid xs={isMobile ? 12 : (latency ? 4 : 6)} sx={{ mt: 2 }}>
                                    <Typography variant="h7">{latency ? 'Latency For' : 'Test Run ID'}:</Typography>
                                    <Typography variant="h8">{latency ? latency.replace('undefined', '') : testRunDetails?.runId}</Typography>
                                </Grid>
                                <Grid xs={latency ? 4 : 6} sx={{ mt: 2 }}>
                                    <Typography variant="h7">Date Time:</Typography>
                                    <Typography variant="h8">{latency ? formatDateTime(testRunDetails?.runTime) : formatDateTime(testRunDetails?.runTime)}</Typography>
                                </Grid>
                                {latency && (
                                    <Grid xs={latency ? 4 : 6} sx={{ textAlign: 'right' }}>
                                        <Typography variant="h7">Test Run ID:</Typography>
                                        <Typography variant="h8">{testRunDetails?.runId}</Typography>
                                    </Grid>
                                )}
                            </Grid>
                        </Box>

                        <Box sx={{ mb: 4 }} style={{overflow:'auto'}}>
                            {testRunDetails?.segmentRuns?.map(segments => (
                                <div className={"db  border-run " + (isMobile ? "mobile-alert-settings" : '')}>
                                    <div className="db header-run">
                                        <Typography variant="h7">Test Segment:</Typography>
                                        <Typography variant="h8">{segments?.runId} - {segments?.segmentName}</Typography>
                                    </div>
                                    <Table className="step-run-table">
                                        <TableHead>
                                            <TableRow>
                                                <TableCell sx={{ width: '5%' }}>STEP ID</TableCell>
                                                <TableCell sx={{ width: '10%' }}>STEP TYPE</TableCell>
                                                <TableCell sx={{ width: '50%' }}>DETAILS</TableCell>
                                                <TableCell sx={{ width: '10%' }}>DETAILED STATUS</TableCell>
                                                <TableCell sx={{ width: '10%' }}>PROMPT LATENCY (SEC)</TableCell>
                                                <TableCell sx={{ width: '10%' }}>UTTERANCE</TableCell>
                                                <TableCell sx={{ width: '5%' }}>ACTIONS</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {segments.stepRuns.map(run => (
                                                <TableRow key={run.runId}>
                                                    <TableCell >{run.stepId}</TableCell>
                                                    <TableCell>[{run.typeDisplayText}]</TableCell>
                                                    <TableCell>
                                                        {run.ui_detail_column.map(col => (
                                                            <div className="db mb-2">
                                                                {formatOutput(run, col)}
                                                            </div>
                                                        ))}
                                                    </TableCell>
                                                    <TableCell className={statuses?.find(s => s.id === +run.runStatus)?.text}>{statuses?.find(s => s.id === +run.runStatus)?.text}</TableCell>
                                                    <TableCell>{!run.bargeInTime ? '' : (+run.bargeInTime)?.toFixed(1)}</TableCell>
                                                    <TableCell>{!run.recordingDuration ? '' : (+run.recordingDuration)?.toFixed(2) + ' sec'} <br />{run.confidence}</TableCell>
                                                    <TableCell>
                                                        {run.recordingId && (
                                                            <img onClick={() => downloadFile(run.recordingId, 'recordingfiles')} className="pointer" src={downloadIcon} alt="Download" />
                                                        )}
                                                    </TableCell>
                                                </TableRow>
                                            ))}
                                        </TableBody>
                                    </Table>

                                </div>
                            ))}
                        </Box>
                    </div>
                </Grid>
            </Grid>
        </div>
    )
}

export default TestRunResult;