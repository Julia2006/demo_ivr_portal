import React, {useEffect} from "react";
import Navbar from "../dashboard/Navbar";
import {Grid} from "@mui/material";
import LeftBar from "../dashboard/LeftBar";
import ReportList from "./ReportList";
import {useDispatch} from "react-redux";
import {getReports, getReportTypes, getTimeframe, groupsList} from "../../actions";

const Report = () => {
    const defaultPage = [0, 1, 0, 0]
    const dispatch = useDispatch()
    useEffect(()=>{
        getReports()(dispatch)
        getReportTypes()(dispatch)
        groupsList()(dispatch)
        getTimeframe()(dispatch)
    }, []) // eslint-disable-line react-hooks/exhaustive-deps


    return (
        <div>
            <Navbar/>
            <Grid container>

                <Grid item>
                    <LeftBar page={defaultPage}/>
                </Grid>
                <Grid item>
                    <ReportList/>
                </Grid>
            </Grid>
        </div>
    );
};

export default Report;
