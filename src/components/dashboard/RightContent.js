import React from "react";
import ContentKpis from "./rightContent/ContentKpis";
import ContentDetails from "./rightContent/ContentDetails";
import Version from "./rightContent/Version";
import ContentHeader from "../mui/ContentHeader";
import { isMobile } from 'react-device-detect';

const RightContent = () => {
    const dashboard = "DASHBOARD"
    return (
        <div style={isMobile ? {paddingBottom:'65px', position: "absolute", top: "0px", left: "20px", right:'10px'} : { position: "absolute", top: "0px", left: "100px", width: `calc(100% - 130px)` }}>
            <ContentHeader title={dashboard} />
            <ContentKpis />
            <ContentDetails />
            <Version />
        </div>
    );
};

export default RightContent;
