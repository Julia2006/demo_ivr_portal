import { Box, Grid, Typography } from "@mui/material";
import { makeStyles } from "@mui/styles";
import React, { useEffect, useState } from "react";
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import WarningAmberIcon from '@mui/icons-material/WarningAmber';
import CancelIcon from '@mui/icons-material/Cancel';
import { useSelector } from "react-redux";
import CaseGroup from "../../../helper/CaseGroup.js";
import sumLogo from "../../../images/2638347_auto_board_car_dash_limit_icon.svg"
import { BrowserView, isMobile, MobileView } from 'react-device-detect';

const useStyles = makeStyles((theme) => ({
    container: {
        paddingTop: theme.spacing(1),
        width: "100%",
        display: "flex",
        justifyContent: "flex-start",
    },
    successRate: {
        width: '100%',
        backgroundColor: theme.palette.background.paper,

    },

    result: {
        background: 'linear-gradient(180deg, #1F9997 0%, #2E6665 57%, #636364 100%)',
        height: "8rem",
        flexGrow: 1,
        textAlign: "center",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        borderRadius: "5px",
    },
    rateTotal: {
        margin: theme.spacing(2),
        flexGrow: 1,
        textAlign: "center",
    },
    testRun: {
        margin: theme.spacing(3, 0, 0),
        flexGrow: 1,
        textAlign: "center",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
    },
    sum: {
        color: "#1F9997",
    },
    success: {
        color: "#FFFFFF",
    },


}));

const ContentKpis = () => {
    const classes = useStyles();
    const [all, setAll] = useState(null)

    const caseGroups = useSelector(state => state.loginReducer.caseGroups)

    useEffect(() => {
        if (caseGroups) {
            let tempAll = {}
            tempAll.total_testRuns = caseGroups.reduce((total, group) => total + group.testRuns, 0);

            tempAll.total_passCount = caseGroups.reduce((total, group) => total + group.counts[0], 0);
            tempAll.total_warnCount = caseGroups.reduce((total, group) => total + group.counts[1], 0);
            tempAll.total_failCount = caseGroups.reduce((total, group) => total + group.counts[2], 0);

            tempAll.overall_passPct = tempAll.total_passCount / tempAll.total_testRuns

            tempAll.total_passPct = tempAll.total_passCount / tempAll.total_testRuns;
            tempAll.total_warnPct = tempAll.total_warnCount / tempAll.total_testRuns;
            tempAll.total_failPct = tempAll.total_failCount / tempAll.total_testRuns;
            setAll(tempAll)
        }

    }, [caseGroups])

    return (
        <div>
            <Grid container wrap="nowrap" className={classes.container}

                style={isMobile ? { display: 'block' } : {}}>

                {isMobile ? (
                    <>
                        <Grid item md={12} xs={12}>
                            <Box
                                mt={1}
                                mr={1}
                                mb={2}
                                ml={0}

                                style={{ height: "49px", boxShadow: '0px 0px 10px #00000029' }}
                            >
                                <div className={classes.successRate}>
                                    <div className={classes.result} style={{ height: '49px' }}>
                                        <Typography gutterBottom variant="body2" color='#FFFFFF'
                                            style={{ fontFamily: "Montserrat", fontWeight: "500", fontSize: "18px" }}>
                                            Overall Test Result Pass Rate
                                        </Typography>
                                    </div>
                                </div>
                            </Box>
                        </Grid>
                        <Grid item xs={12} md={12}>
                            <Box
                                bgcolor='background.paper'
                                mt={1}
                                mr={1}
                                mb={1}
                                ml={0}
                                p={3}
                                style={
                                    isMobile ? { height: '81px', justifyContent: 'center', alignItems: 'center', margin: 0, padding: 0, display: 'flex', borderRadius: "5px", boxShadow: '0px 0px 10px #00000029' } :
                                        { height: "9rem", borderRadius: "5px", boxShadow: '0px 0px 10px #00000029' }}
                            >
                                <div className={classes.successRate}>
                                    <div className={classes.rateTotal}>
                                        <Typography gutterBottom variant='h3' color='#1F9997' style={{ fontFamily: "Montserrat", fontWeight: "700", fontSize: '28px' }}>
                                            {all && !isNaN(all.overall_passPct) ? Math.round(100 * all.overall_passPct) : null}%
                                        </Typography>
                                    </div>
                                </div>
                            </Box>
                        </Grid>
                        <Grid item xs={12} md={12} className='dashboard-mobile-kpi'>
                            <Box
                                bgcolor='background.paper'
                                m={1}
                                p={4}
                                style={{ height: "120px", borderRadius: "5px", boxShadow: ' 0px 0px 10px #00000017' }}
                            >
                                <div className="dashboard-flex-totals">
                                    <Typography className={classes.sum}>
                                        <img src={sumLogo} alt="pic" style={{ width: "25px" }} />
                                    </Typography>
                                    <Typography gutterBottom variant='body2' color="#1F9997"
                                        style={{ fontFamily: "Montserrat", fontWeight: "700", fontSize: '10px' }}>
                                        Total Test Runs
                                    </Typography>
                                    
                                    {!isMobile && (
                                        <br />
                                    )}

                                    <Typography gutterBottom variant='h4' color='#636344' style={{ fontFamily: "Montserrat", fontWeight: "700", fontSize: '16px' }}>
                                        {all && all.total_testRuns}
                                    </Typography>
                                </div>
                            </Box>
                            <Box
                                m={1}
                                p={3}
                                style={{

                                    height: "120px",
                                    background: "linear-gradient(180deg, #4CAF50 0%, #337E36 100%)",
                                    borderRadius: "5px",
                                    boxShadow: ' 0px 0px 10px #00000017'
                                }}
                            >
                                <div className="dashboard-flex-totals">
                                    <Typography className={classes.success}>
                                        <CheckCircleOutlineIcon style={{ fontSize: 25 }} />
                                    </Typography>

                                    <Typography gutterBottom variant='body2' color="#FFFFFF"
                                        style={{
                                            fontFamily: "Montserrat",
                                            fontWeight: "600",
                                            fontSize: "10px"
                                        }}
                                    >
                                        Pass
                                    </Typography>
                                    <Typography gutterBottom color='#FFFFFF'
                                        style={{
                                            fontFamily: "Montserrat",
                                            fontWeight: "700", fontSize: "16px"
                                        }}
                                    >
                                        {all && !isNaN(all.total_passPct) && Math.round(100 * all.total_passPct)}%
                                    </Typography>
                                    <Typography gutterBottom color='#FFFFFF'
                                        style={{
                                            fontFamily: "Montserrat",
                                            fontSize: "14px",
                                            fontWeight: '500'
                                        }}>
                                        {all && all.total_passCount}
                                    </Typography>
                                </div>
                            </Box>
                            <Box
                                m={1}
                                p={3}
                                style={{

                                    height: "120px",
                                    background: "linear-gradient(180deg, #E56C35 0%, #933A13 100%)",
                                    borderRadius: "5px", boxShadow: ' 0px 0px 10px #00000017'
                                }}
                            >
                                <div className="dashboard-flex-totals">
                                    <Typography className={classes.success}>
                                        <WarningAmberIcon style={{ fontSize: 25 }} />
                                    </Typography>

                                    <Typography gutterBottom variant='body2' color="#FFFFFF"
                                        style={{
                                            fontFamily: "Montserrat",
                                            fontWeight: "600",
                                            fontSize: "10px"
                                        }}>
                                        Warning
                                    </Typography>
                                    <Typography gutterBottom color='#FFFFFF'
                                        style={{
                                            fontFamily: "Montserrat",
                                            fontWeight: "700", fontSize: "16px"
                                        }}
                                    >
                                        {all && !isNaN(all.total_warnPct) && Math.round(100 * all.total_warnPct)}%
                                    </Typography>
                                    <Typography gutterBottom color='#FFFFFF'
                                        style={{
                                            fontFamily: "Montserrat",
                                            fontSize: "14px",
                                            fontWeight: '500'
                                        }}>
                                        {all && all.total_warnCount}
                                    </Typography>
                                </div>
                            </Box>
                            <Box
                                m={1}
                                p={3}
                                style={{

                                    height: "120px",
                                    background: "linear-gradient(180deg, #ED1F24 0%, #973D3F 100%",
                                    borderRadius: "5px", boxShadow: ' 0px 0px 10px #00000017'
                                }}
                            >
                                <div className="dashboard-flex-totals">
                                    <Typography className={classes.success}>
                                        <CancelIcon style={{ fontSize: 25 }} />
                                    </Typography>

                                    <Typography gutterBottom variant='body2' color="#FFFFFF"
                                        style={{
                                            fontFamily: "Montserrat",
                                            fontWeight: "600",
                                            fontSize: "10px"
                                        }}
                                    >
                                        Failure
                                    </Typography>
                                    <Typography gutterBottom color='#FFFFFF'
                                        style={{
                                            fontFamily: "Montserrat",
                                            fontWeight: "700", fontSize: "16px"
                                        }}
                                    >
                                        {all && !isNaN(all.total_failPct) && Math.round(100 * all.total_failPct)}%
                                    </Typography>
                                    <Typography gutterBottom color='#FFFFFF'
                                        style={{
                                            fontFamily: "Montserrat",
                                            fontSize: "14px",
                                            fontWeight: '500'
                                        }}>
                                        {all && all.total_failCount}
                                    </Typography>
                                </div>
                            </Box>
                        </Grid>
                    </>
                ) : (

                    <>
                        <Grid item xs={4}>
                            <Box
                                mt={1}
                                mr={1}
                                mb={2}
                                ml={0}

                                style={{ height: "8rem", boxShadow: '0px 0px 10px #00000029' }}
                            >
                                <div className={classes.successRate}>
                                    <div className={classes.result}>
                                        <Typography gutterBottom variant="body2" color='#FFFFFF'
                                            style={{ fontFamily: "Montserrat", fontWeight: "500", fontSize: "1.5rem" }}>
                                            Overall Test Result Pass Rate
                                        </Typography>
                                    </div>
                                </div>
                            </Box>

                            {/* Overall Test Result Pass Rate ------------------------- */}
                            <Box
                                bgcolor='background.paper'
                                mt={1}
                                mr={1}
                                mb={1}
                                ml={0}
                                p={3}
                                style={{ height: "9rem", borderRadius: "5px", boxShadow: '0px 0px 10px #00000029' }}
                            >
                                <div className={classes.successRate}>
                                    <div className={classes.rateTotal}>
                                        <Typography gutterBottom variant='h3' color='#1F9997' style={{ fontFamily: "Montserrat", fontWeight: "700", fontSize: '54px' }}>
                                            {all && !isNaN(all.overall_passPct) ? Math.round(100 * all.overall_passPct) : null}%
                                        </Typography>
                                    </div>
                                </div>
                            </Box>
                        </Grid>
                        <Grid item xs={2}>
                            {/* Total Test Runs --------------------------------------- */}
                            <Box
                                bgcolor='background.paper'
                                m={1}
                                p={4}
                                style={{ height: "18rem", borderRadius: "5px", boxShadow: ' 0px 0px 10px #00000017' }}
                            >
                                <div className={classes.testRun}>
                                    <Typography className={classes.sum}>
                                        <img src={sumLogo} alt="pic" style={{ width: "50px" }} />
                                    </Typography>
                                    <br />
                                    <Typography gutterBottom variant='body2' color="#1F9997"
                                        style={{ fontFamily: "Montserrat", fontWeight: "700", fontSize: '1rem' }}>
                                        Total Test Runs
                                    </Typography>
                                    <br />
                                    <Typography gutterBottom variant='h4' color='#636344' style={{ fontFamily: "Montserrat", fontWeight: "700", fontSize: '38px' }}>
                                        {all && all.total_testRuns}
                                    </Typography>
                                </div>
                            </Box>
                        </Grid>
                        <Grid item xs={2}>
                            {/* Passes ------------------------------------------------ */}
                            <Box
                                m={1}
                                p={3}
                                style={{

                                    height: "18rem",
                                    background: "linear-gradient(180deg, #4CAF50 0%, #337E36 100%)",
                                    borderRadius: "5px",
                                    boxShadow: ' 0px 0px 10px #00000017'
                                }}
                            >
                                <div className={classes.testRun}>
                                    <Typography className={classes.success}>
                                        <CheckCircleOutlineIcon style={{ fontSize: 50 }} />
                                    </Typography>

                                    <Typography gutterBottom variant='body2' color="#FFFFFF"
                                        style={{
                                            fontFamily: "Montserrat",
                                            fontWeight: "600",
                                            fontSize: "1rem"
                                        }}
                                    >
                                        Pass
                                    </Typography>
                                    <Typography gutterBottom color='#FFFFFF'
                                        style={{
                                            fontFamily: "Montserrat",
                                            fontWeight: "700", fontSize: "38px"
                                        }}
                                    >
                                        {all && !isNaN(all.total_passPct) && Math.round(100 * all.total_passPct)}%
                                    </Typography>
                                    <Typography gutterBottom color='#FFFFFF'
                                        style={{
                                            fontFamily: "Montserrat",
                                            fontSize: "34px",
                                            fontWeight: '500'
                                        }}>
                                        {all && all.total_passCount}
                                    </Typography>
                                </div>
                            </Box>
                        </Grid>
                        <Grid item xs={2}>
                            {/* Warnings ---------------------------------------------- */}
                            <Box
                                m={1}
                                p={3}
                                style={{

                                    height: "18rem",
                                    background: "linear-gradient(180deg, #E56C35 0%, #933A13 100%)",
                                    borderRadius: "5px", boxShadow: ' 0px 0px 10px #00000017'
                                }}
                            >
                                <div className={classes.testRun}>
                                    <Typography className={classes.success}>
                                        <WarningAmberIcon style={{ fontSize: 50 }} />
                                    </Typography>

                                    <Typography gutterBottom variant='body2' color="#FFFFFF"
                                        style={{
                                            fontFamily: "Montserrat",
                                            fontWeight: "600",
                                            fontSize: "16"
                                        }}>
                                        Warning
                                    </Typography>
                                    <Typography gutterBottom color='#FFFFFF'
                                        style={{
                                            fontFamily: "Montserrat",
                                            fontWeight: "700", fontSize: "38px"
                                        }}
                                    >
                                        {all && !isNaN(all.total_warnPct) && Math.round(100 * all.total_warnPct)}%
                                    </Typography>
                                    <Typography gutterBottom color='#FFFFFF'
                                        style={{
                                            fontFamily: "Montserrat",
                                            fontSize: "34px",
                                            fontWeight: '500'
                                        }}>
                                        {all && all.total_warnCount}
                                    </Typography>
                                </div>
                            </Box>
                        </Grid>
                        <Grid item xs={2}>
                            {/* Failures ---------------------------------------------- */}
                            <Box
                                m={1}
                                p={3}
                                style={{

                                    height: "18rem",
                                    background: "linear-gradient(180deg, #ED1F24 0%, #973D3F 100%",
                                    borderRadius: "5px", boxShadow: ' 0px 0px 10px #00000017'
                                }}
                            >
                                <div className={classes.testRun}>
                                    <Typography className={classes.success}>
                                        <CancelIcon style={{ fontSize: 50 }} />
                                    </Typography>

                                    <Typography gutterBottom variant='body2' color="#FFFFFF"
                                        style={{
                                            fontFamily: "Montserrat",
                                            fontWeight: "600",
                                            fontSize: "1rem"
                                        }}
                                    >
                                        Failure
                                    </Typography>
                                    <Typography gutterBottom color='#FFFFFF'
                                        style={{
                                            fontFamily: "Montserrat",
                                            fontWeight: "700", fontSize: "38px"
                                        }}
                                    >
                                        {all && !isNaN(all.total_failPct) && Math.round(100 * all.total_failPct)}%
                                    </Typography>
                                    <Typography gutterBottom color='#FFFFFF'
                                        style={{
                                            fontFamily: "Montserrat",
                                            fontSize: "34px",
                                            fontWeight: '500'
                                        }}>
                                        {all && all.total_failCount}
                                    </Typography>
                                </div>
                            </Box>
                        </Grid>
                    </>
                )}
            </Grid>
        </div>
    );
};

export default ContentKpis;
