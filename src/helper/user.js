const getUser = () => {
    const user = localStorage.getItem('user');

    if (user) {
        return JSON.parse(user)?.data;
    }

    return null;
}

export const getUserId = () => {
    return getUser()?.id;
}

export const isAdmin = () => {
    return getUser()?.roles[0] !== 'ROLE_USER'
}


export const getRole = () => {
    return getUser()?.roles
}